const http = require('http');

//Mock Database

let directory = [
    {"name": 'Brandon', "email": 'brandon@email.com'},
    {"name": 'Jobert', "email": 'jobert@email.com'}
]

const port = 3000;

http.createServer( (request, response) => {

    /*
     Add a route here for GET method that 
     will retrived the content of our database.
     */

     // set the status to 200
     // application/json it sets  the response to JSON Data type
     if(request.url === '/users' && request.method === 'GET'){
        response.writeHead(200, {'Content-Type': 'application/json'});

        //Input has to be data type STRING
        // hence the JSON.stringify(method)
        response.write(JSON.stringify(directory));

        // To end the route we add response.end
        response.end()
     }
     else if(request.url === '/users' && request.method === 'POST'){
        let requestBody = '';

        request.on('data', data => requestBody += data);

        request.on('end', ()=> {
            console.log(typeof(requestBody));
            console.log(requestBody);

            requestBody= JSON.parse(requestBody);
            console.log(typeof(requestBody));

            let newUser = {
                "name" : requestBody.name,
                "email" : requestBody.email
            }

            console.log(newUser);

            directory.push(newUser);

            response.writeHead(200, {'Content-Type': 'application/JSON'});
            
            response.write( JSON.stringify(newUser));
            response.end()

        })

     }

}).listen(port);

console.log(`Server is running at port ${port}!`);